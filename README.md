# 'AddToBag' & 'NewsletterSubscription' for ZALORA

> Assignments for front-end developer interview

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

```
After starting the server, you can find the answers in the following url

### Assignment 1: Reproduce the add-to-bag button of ZALORA mobile website
http://localhost:8080/#/addToBag


### Assignment 2: Redesign the newsletter subscription slider on the desktop
http://localhost:8080/#/newsletterSubscription
