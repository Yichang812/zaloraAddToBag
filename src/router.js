// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Router from 'vue-router'
import addToBag from './components/AddToBag.vue'
import newsletterSubscription from './components/NewsletterSubscription.vue'

Vue.config.productionTip = false

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/addToBag',
      name: 'addToBag',
      component: addToBag
    },
    {
      path: '/newsletterSubscription',
      name: 'newsletterSubscription',
      component: newsletterSubscription
    }
  ]
})
